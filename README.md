CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Introspect is an ultra minimal landing page template with a large cover photo.
The theme is fully responsive, built with bootstrap.

 * For a full description of the theme, visit the project page:
   https://www.drupal.org/project/introspect

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/introspect


REQUIREMENTS
------------

This theme requires the following theme:

 * Bootstrap (https://www.drupal.org/project/bootstrap)


INSTALLATION
--------------------------

Note: As this theme is based on bootstrap you should download and enable
Drupal Bootstrap project.

To install Introspect theme, follow the steps mentioned below.

 - Theme file can be downloaded from the link
   https://www.drupal.org/project/introspect
 - Extract the downloaded file to the themes directory.
 - Goto Admin > Appearance, find Introspect theme and choose 'Install and
   set as default' option.
 - You have now enabled your theme.


CONFIGURATION
-------------

The theme sections can be customized from the theme settings in
admin area.


MAINTAINERS
-----------

Current maintainers:
 * Zyxware - https://www.drupal.org/u/zyxware
